package com.schibsted.android.chatbot.domain;

import com.schibsted.android.chatbot.domain.validator.NameValidator;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Lucian on 1/13/2017.
 */

public class NameValidatorUnitTest {
    private NameValidator nameValidator;

    @Before
    public void setup() {
        nameValidator = new NameValidator();
    }

    @Test
    public void testValidator() {
        assertFalse(nameValidator.isValid("Lucian"));
        assertFalse(nameValidator.isValid(" Lucian"));
        assertFalse(nameValidator.isValid("Lucian  "));
        assertTrue(nameValidator.isValid("Lucian Galea"));
    }
}
