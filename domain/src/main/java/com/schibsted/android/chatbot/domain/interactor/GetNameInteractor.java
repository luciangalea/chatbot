package com.schibsted.android.chatbot.domain.interactor;

import com.schibsted.android.chatbot.data.repository.DataRepository;
import com.schibsted.android.chatbot.domain.interactor.response.Response;
import com.schibsted.android.chatbot.domain.interactor.thread.InteractorThread;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/13/2017.
 */

public class GetNameInteractor extends BaseInteractor<Response<String>> {

    @Inject
    GetNameInteractor(@Named("observeThread") InteractorThread observeThread,
                      @Named("subscribeThread") InteractorThread subscribeThread,
                      DataRepository repository) {
        super(observeThread, subscribeThread, repository);
    }

    @Override
    protected Observable<Response<String>> operation() {
        return repository.getName().map(Response::ok);
    }
}
