package com.schibsted.android.chatbot.domain.validator;

/**
 * Created by Lucian on 1/12/2017.
 */

public interface Validator {
    boolean isValid(String name);
}
