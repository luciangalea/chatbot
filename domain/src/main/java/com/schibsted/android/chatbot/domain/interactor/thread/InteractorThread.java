package com.schibsted.android.chatbot.domain.interactor.thread;

import io.reactivex.Scheduler;

/**
 * Created by Lucian on 1/13/2017.
 */

public interface InteractorThread {
    Scheduler scheduler();
}
