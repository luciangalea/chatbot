package com.schibsted.android.chatbot.domain.interactor.response;

/**
 * Created by Lucian on 1/13/2017.
 */

public class Response<T> {
    private boolean success;
    //will be null if success is true
    private String errorMessage;

    private T data;

    private Response(boolean success, String errorMessage, T data) {
        this.success = success;
        this.errorMessage = errorMessage;
        this.data = data;
    }

    public boolean success() {
        return success;
    }

    public String errorMessage() {
        return errorMessage;
    }

    public T data() {
        return data;
    }

    public static Response<Void> ok() {
        return new Response<>(true, null, null);
    }

    public static <T> Response<T> ok(T data) {
        return new Response<>(true, null, data);
    }

    public static Response<Void> error(String errorMessage) {
        return new Response<>(false, errorMessage, null);
    }

    public static <T> Response<T> error(String errorMessage, T data) {
        return new Response<>(false, errorMessage, data);
    }
}
