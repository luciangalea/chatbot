package com.schibsted.android.chatbot.domain.validator;


/**
 * Created by Lucian on 1/13/2017.
 */

public class NameValidator implements Validator {
    @Override
    public boolean isValid(String name) {
        String[] parts = name.split(" ");
        return parts.length == 2 && !isEmpty(parts[0].trim()) && !isEmpty(parts[1].trim());
    }

    //TextUtils.isEmpty() is not being used to avoid any dependency on the Android framework
    //in order to allow faster unit testing
    private boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
}
