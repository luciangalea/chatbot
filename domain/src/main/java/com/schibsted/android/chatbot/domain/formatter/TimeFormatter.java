package com.schibsted.android.chatbot.domain.formatter;

import org.threeten.bp.temporal.TemporalAccessor;

/**
 * Created by Lucian on 1/13/2017.
 */

public interface TimeFormatter {
    String formatTime(TemporalAccessor temporalAccessor);
}
