package com.schibsted.android.chatbot.domain.formatter;

import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.temporal.TemporalAccessor;

/**
 * Created by Lucian on 1/13/2017.
 */

public class TimeFormatterImpl implements TimeFormatter {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm'h'");
    @Override
    public String formatTime(TemporalAccessor temporalAccessor) {
        return FORMATTER.format(temporalAccessor);
    }
}
