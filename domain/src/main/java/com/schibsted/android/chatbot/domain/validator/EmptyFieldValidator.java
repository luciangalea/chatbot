package com.schibsted.android.chatbot.domain.validator;

import static android.text.TextUtils.isEmpty;

/**
 * Created by Lucian on 1/13/2017.
 */

public class EmptyFieldValidator implements Validator {
    @Override
    public boolean isValid(String name) {
        return !isEmpty(name);
    }
}
