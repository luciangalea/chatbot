package com.schibsted.android.chatbot.domain.interactor;

import com.schibsted.android.chatbot.data.repository.DataRepository;
import com.schibsted.android.chatbot.domain.interactor.response.Response;
import com.schibsted.android.chatbot.domain.interactor.thread.InteractorThread;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/13/2017.
 */

public class LogoutInteractor extends BaseInteractor<Response<Void>> {
    private String logoutErrorMessage;

    @Inject
    LogoutInteractor(@Named("observeThread") InteractorThread observeThread,
                     @Named("subscribeThread") InteractorThread subscribeThread,
                     DataRepository repository,
                     @Named("logoutErrorMessage") String logoutErrorMessage) {
        super(observeThread, subscribeThread, repository);

        this.logoutErrorMessage = logoutErrorMessage;
    }

    @Override
    protected Observable<Response<Void>> operation() {
        return repository.logout().map(success -> success ?
                Response.ok() : Response.error(logoutErrorMessage));
    }
}
