package com.schibsted.android.chatbot.domain.interactor;

import com.schibsted.android.chatbot.data.entity.ChatMessage;
import com.schibsted.android.chatbot.data.repository.DataRepository;
import com.schibsted.android.chatbot.domain.interactor.thread.InteractorThread;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/13/2017.
 */

public class GetMessagesInteractor extends BaseInteractor<List<ChatMessage>> {
    @Inject
    GetMessagesInteractor(@Named("observeThread") InteractorThread observeThread,
                          @Named("subscribeThread") InteractorThread subscribeThread,
                          DataRepository repository) {
        super(observeThread, subscribeThread, repository);
    }

    @Override
    protected Observable<List<ChatMessage>> operation() {
        return repository.getMessages();
    }
}
