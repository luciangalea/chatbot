package com.schibsted.android.chatbot.domain.interactor;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.schibsted.android.chatbot.data.repository.DataRepository;
import com.schibsted.android.chatbot.domain.interactor.response.Response;
import com.schibsted.android.chatbot.domain.interactor.thread.InteractorThread;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/13/2017.
 */

public class LoginInteractor extends BaseInteractor<Response<Void>> {

    private String nameUsedError;
    private String loginError;
    private String name;

    //The error messages are being injected here, as they shouldn't be the interactor responsibility
    //In a real environment, they would probably be provided by the API.
    @Inject
    LoginInteractor(@Named("observeThread") InteractorThread observeThread,
                    @Named("subscribeThread") InteractorThread subscribeThread,
                    DataRepository repository,
                    @Named("nameUsedError") String nameUsedError,
                    @Named("loginError") String loginError) {
        super(observeThread, subscribeThread, repository);

        this.nameUsedError = nameUsedError;
        this.loginError = loginError;
    }

    public LoginInteractor withData(String name) {
        this.name = name;
        return this;
    }

    @Override
    protected Observable<Response<Void>> operation() {
        return repository.getUsedNames().flatMap(names -> {
            if (nameAlreadyUsed(names)) {
                return Observable.just(Response.error(nameUsedError));
            } else {
                return repository.login(name).map(success ->
                        success ? Response.ok() : Response.error(loginError));
            }
        });
    }

    private boolean nameAlreadyUsed(List<String> names) {
        return Stream.of(names).map(String::toUpperCase)
                .collect(Collectors.toList())
                .contains(name);
    }
}
