package com.schibsted.android.chatbot.domain.interactor;

import com.schibsted.android.chatbot.data.repository.DataRepository;
import com.schibsted.android.chatbot.domain.interactor.thread.InteractorThread;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/13/2017.
 */

public abstract class BaseInteractor<T> {
    private InteractorThread observeThread;
    private InteractorThread subscribeThread;
    protected DataRepository repository;

    BaseInteractor(InteractorThread observeThread, InteractorThread subscribeThread, DataRepository repository) {
        this.observeThread = observeThread;
        this.subscribeThread = subscribeThread;
        this.repository = repository;
    }

    public Observable<T> execute() {
        return operation()
                .subscribeOn(subscribeThread.scheduler())
                .observeOn(observeThread.scheduler());
    }

    protected abstract Observable<T> operation();
}
