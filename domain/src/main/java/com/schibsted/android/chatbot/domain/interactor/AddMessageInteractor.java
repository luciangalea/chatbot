package com.schibsted.android.chatbot.domain.interactor;

import android.support.v4.util.Pair;

import com.schibsted.android.chatbot.data.entity.ChatMessage;
import com.schibsted.android.chatbot.data.repository.DataRepository;
import com.schibsted.android.chatbot.domain.formatter.TimeFormatter;
import com.schibsted.android.chatbot.domain.interactor.response.Response;
import com.schibsted.android.chatbot.domain.interactor.thread.InteractorThread;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/13/2017.
 */

public class AddMessageInteractor extends BaseInteractor<Response<ChatMessage>>{
    private TimeFormatter formatter;
    private String message;
    private String errorMessage;

    @Inject
    AddMessageInteractor(@Named("observeThread") InteractorThread observeThread,
                                @Named("subscribeThread") InteractorThread subscribeThread,
                                DataRepository repository,
                                TimeFormatter formatter,
                                @Named("messageSendError") String errorMessage) {
        super(observeThread, subscribeThread, repository);

        this.formatter = formatter;
        this.errorMessage = errorMessage;
    }

    public AddMessageInteractor withData(String message) {
        this.message = message;
        return this;
    }

    @Override
    protected Observable<Response<ChatMessage>> operation() {
        return Observable.zip(
                repository.getName(),
                repository.getMessages(),
                Pair::new
        ).flatMap(pair -> {
            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setContent(message);
            chatMessage.setUsername(pair.first);
            chatMessage.setImageUrl("https://pressdispensary.co.uk/q991744/images/aco_bot.jpg");
            chatMessage.setTime(formatter.formatTime(LocalDateTime.now()));
            pair.second.add(chatMessage);

            return repository.updateMessages(pair.second)
                    .map(success -> success ? Response.ok(chatMessage) : Response.error(errorMessage, null));
        });
    }
}
