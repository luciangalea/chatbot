package com.schibsted.android.chatbot.data;

import com.schibsted.android.chatbot.data.cache.Cache;
import com.schibsted.android.chatbot.data.configuration.Configuration;
import com.schibsted.android.chatbot.data.entity.ChatMessage;
import com.schibsted.android.chatbot.data.entity.ChatMessagesResult;
import com.schibsted.android.chatbot.data.repository.DataRepository;
import com.schibsted.android.chatbot.data.store.MessagesStore;
import com.schibsted.android.chatbot.data.store.UserStore;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Lucian on 1/13/2017.
 */

public class RepositoryTest {
    private static final long CACHE_EXPIRATION = 24L * 3600L * 1000L;
    @Mock
    MessagesStore messagesStore;
    @Mock
    UserStore userStore;
    @Mock
    Cache diskCache;
    @Mock
    Cache memoryCache;
    @Mock
    Configuration configuration;

    private DataRepository repository;
    private ChatMessagesResult memoryResult;
    private ChatMessagesResult diskResult;
    private ChatMessagesResult storeResult;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        repository = new DataRepository(messagesStore, userStore, diskCache, memoryCache, configuration);
        memoryResult = mockResult();
        diskResult = mockResult();
        storeResult = memoryResult;

        when(configuration.getCacheDurationMillis()).thenReturn(CACHE_EXPIRATION);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testMessagesReadFromDiskCache() {
        diskResult.setTimestamp(System.currentTimeMillis());
        when(memoryCache.getMessages()).thenReturn(Observable.just(ChatMessagesResult.dummy()));
        when(diskCache.getMessages()).thenReturn(Observable.just(diskResult));
        when(messagesStore.getMessages()).thenReturn(Observable.just(storeResult).delay(500, TimeUnit.MILLISECONDS));

        TestObserver<List<ChatMessage>> observer = new TestObserver<>();
        repository.getMessages().subscribeWith(observer);
        observer.awaitTerminalEvent();

        verify(memoryCache).storeMessages(diskResult);
        verify(diskCache, never()).storeMessages(any(ChatMessagesResult.class));

        observer.assertResult(diskResult.getMessages());
        observer.assertComplete();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testMessagesReadFromMemoryCache() {
        when(memoryCache.getMessages()).thenReturn(Observable.just(memoryResult));
        when(diskCache.getMessages()).thenReturn(Observable.just(diskResult).delay(200, TimeUnit.MILLISECONDS));
        when(messagesStore.getMessages()).thenReturn(Observable.just(storeResult).delay(500, TimeUnit.MILLISECONDS));

        TestObserver<List<ChatMessage>> observer = new TestObserver<>();
        repository.getMessages().subscribeWith(observer);
        observer.awaitTerminalEvent();

        verify(memoryCache, never()).storeMessages(any(ChatMessagesResult.class));
        observer.assertResult(memoryResult.getMessages());
        observer.assertComplete();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testMessagesReadFromDataStoreWhenCacheExpired() {
        diskResult.setTimestamp(System.currentTimeMillis() - 2 * CACHE_EXPIRATION);
        when(memoryCache.getMessages()).thenReturn(Observable.just(ChatMessagesResult.dummy()));
        when(diskCache.getMessages()).thenReturn(Observable.just(diskResult));
        when(messagesStore.getMessages()).thenReturn(Observable.just(storeResult).delay(500, TimeUnit.MILLISECONDS));

        TestObserver<List<ChatMessage>> observer = new TestObserver<>();
        repository.getMessages().subscribeWith(observer);
        observer.awaitTerminalEvent();

        verify(memoryCache).storeMessages(storeResult);
        verify(diskCache).storeMessages(storeResult);
        observer.assertResult(storeResult.getMessages());
        observer.assertComplete();
    }

    private ChatMessagesResult mockResult() {
        List<ChatMessage> messages = new ArrayList<>();
        ChatMessagesResult result = new ChatMessagesResult();
        result.setMessages(messages);

        return result;
    }
}
