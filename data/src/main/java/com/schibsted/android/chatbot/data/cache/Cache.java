package com.schibsted.android.chatbot.data.cache;

import com.schibsted.android.chatbot.data.entity.ChatMessagesResult;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/12/2017.
 */

public interface Cache {
    Observable<String> getName();

    void storeName(String name);

    void storeMessages(ChatMessagesResult messages);

    Observable<ChatMessagesResult> getMessages();

    void clear();
}
