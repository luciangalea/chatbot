package com.schibsted.android.chatbot.data.store;

import com.schibsted.android.chatbot.data.entity.ChatMessagesResult;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/12/2017.
 */

public class CloudMessagesStore implements MessagesStore {
    private MessagesAPI api;

    public CloudMessagesStore(MessagesAPI api) {
        this.api = api;
    }

    @Override
    public Observable<ChatMessagesResult> getMessages() {
        return api.getMessages();
    }
}
