package com.schibsted.android.chatbot.data.cache.disk;

import java.io.Serializable;

/**
 * Created by Lucian on 1/12/2017.
 */

public interface DiskProxy {
    void writeToFile(String name, Serializable serializable);

    <T extends Serializable> T readFromFile(String name, Class<T> clazz);

    void deleteFile(String name);
}
