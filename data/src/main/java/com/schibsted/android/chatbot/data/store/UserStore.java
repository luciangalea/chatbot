package com.schibsted.android.chatbot.data.store;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/13/2017.
 */

public interface UserStore {
    Observable<List<String>> getUsedNames();
}
