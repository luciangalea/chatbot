package com.schibsted.android.chatbot.data.configuration;

/**
 * Created by Lucian on 1/12/2017.
 */

public class ConfigurationImpl implements Configuration {
    @Override
    public long getCacheDurationMillis() {
        return 12L * 3600L * 1000L;
    }
}
