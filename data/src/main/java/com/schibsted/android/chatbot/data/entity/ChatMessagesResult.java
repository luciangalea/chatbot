package com.schibsted.android.chatbot.data.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Lucian on 1/12/2017.
 */

public class ChatMessagesResult implements Serializable {
    private static ChatMessagesResult dummy;

    @SerializedName("chats")
    private List<ChatMessage> messages;

    //used for invalidating the disk cache
    private long timestamp;

    public List<ChatMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<ChatMessage> messages) {
        this.messages = messages;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public static ChatMessagesResult dummy() {
        if (dummy == null) {
            dummy = new ChatMessagesResult();
        }
        return dummy;
    }
}

