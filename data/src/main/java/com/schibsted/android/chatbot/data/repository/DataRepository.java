package com.schibsted.android.chatbot.data.repository;

import com.schibsted.android.chatbot.data.cache.Cache;
import com.schibsted.android.chatbot.data.configuration.Configuration;
import com.schibsted.android.chatbot.data.entity.ChatMessage;
import com.schibsted.android.chatbot.data.entity.ChatMessagesResult;
import com.schibsted.android.chatbot.data.store.MessagesStore;
import com.schibsted.android.chatbot.data.store.UserStore;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/12/2017.
 */

public class DataRepository {
    private MessagesStore messagesStore;
    private UserStore userStore;
    private Cache diskCache;
    private Cache memoryCache;
    private Configuration configuration;

    @Inject
    public DataRepository(MessagesStore messagesStore,
                          UserStore userStore,
                          @Named("disk") Cache diskCache,
                          @Named("memory") Cache memoryCache,
                          Configuration configuration) {
        this.messagesStore = messagesStore;
        this.userStore = userStore;
        this.diskCache = diskCache;
        this.memoryCache = memoryCache;
        this.configuration = configuration;
    }

    public Observable<String> getName() {
        return Observable.merge(
                diskCache.getName().doOnNext(s -> {
                    if (s.length() > 0) {
                        memoryCache.storeName(s);
                    }
                }),
                memoryCache.getName().filter(s -> s.length() > 0)
        ).take(1);
    }


    public Observable<List<ChatMessage>> getMessages() {
        return Observable.merge(
                memoryCache.getMessages().filter(result -> result != ChatMessagesResult.dummy()),
                diskCache.getMessages().filter(result -> result != ChatMessagesResult.dummy()
                        && System.currentTimeMillis() - result.getTimestamp() < configuration.getCacheDurationMillis())
                        .doOnNext(memoryCache::storeMessages),
                messagesStore.getMessages().doOnNext(result -> {
                    result.setTimestamp(System.currentTimeMillis());
                    diskCache.storeMessages(result);
                    memoryCache.storeMessages(result);
                })
        ).take(1).map(ChatMessagesResult::getMessages);
    }

    public Observable<Boolean> updateMessages(List<ChatMessage> messages) {
        return Observable.create(e -> {
            ChatMessagesResult result = new ChatMessagesResult();
            result.setMessages(messages);
            result.setTimestamp(System.currentTimeMillis());
            diskCache.storeMessages(result);
            memoryCache.storeMessages(result);

            e.onNext(true);
            e.onComplete();
        });
    }


    //in a real environment, these shouldn't be cached and be fetched every time from the API
    //to make sure the username is available
    public Observable<List<String>> getUsedNames() {
        return userStore.getUsedNames();
    }

    public Observable<Boolean> login(String name) {
        return Observable.create(e -> {
            diskCache.storeName(name);
            memoryCache.storeName(name);

            e.onNext(true);
            e.onComplete();
        });

    }

    public Observable<Boolean> logout() {
        return Observable.create(e -> {
            diskCache.clear();
            memoryCache.clear();

            e.onNext(true);
            e.onComplete();
        });
    }
}
