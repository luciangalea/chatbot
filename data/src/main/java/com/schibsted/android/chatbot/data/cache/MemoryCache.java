package com.schibsted.android.chatbot.data.cache;

import com.schibsted.android.chatbot.data.entity.ChatMessagesResult;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/12/2017.
 */

public class MemoryCache implements Cache {
    private String name;
    private ChatMessagesResult messagesResult;

    @Override
    public Observable<String> getName() {
        //emitting null will lead to an exception, that's why we will just emit an empty string which will be filtered
        if (name != null) {
            return Observable.just(name);
        } else {
            return Observable.just("");
        }
    }

    @Override
    public void storeName(String name) {
        this.name = name;
    }

    @Override
    public void storeMessages(ChatMessagesResult messages) {
        this.messagesResult = messages;
    }

    @Override
    public Observable<ChatMessagesResult> getMessages() {
        return Observable.just(messagesResult != null ? messagesResult : ChatMessagesResult.dummy());
    }

    @Override
    public void clear() {
        this.name = null;
        this.messagesResult = null;
    }
}
