package com.schibsted.android.chatbot.data.log;

/**
 * Created by Lucian on 1/12/2017.
 */

public interface Logger {
    void logError(String tag, String message, Throwable throwable);
}
