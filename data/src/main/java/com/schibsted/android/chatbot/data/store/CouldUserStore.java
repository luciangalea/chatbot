package com.schibsted.android.chatbot.data.store;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/13/2017.
 */

public class CouldUserStore implements UserStore {
    @Override
    public Observable<List<String>> getUsedNames() {
        return Observable.just(
                Arrays.asList("Carrie", "Anthony", "Eleanor", "Rodney", "Olivia",
                        "Merve", "Lilly"));
    }
}
