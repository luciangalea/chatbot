package com.schibsted.android.chatbot.data.store;

import com.schibsted.android.chatbot.data.entity.ChatMessagesResult;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/12/2017.
 */

public interface MessagesStore {
    Observable<ChatMessagesResult> getMessages();
}
