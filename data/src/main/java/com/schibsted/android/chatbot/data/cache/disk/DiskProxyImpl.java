package com.schibsted.android.chatbot.data.cache.disk;

import android.content.Context;

import com.schibsted.android.chatbot.data.log.Logger;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by Lucian on 1/12/2017.
 */

public class DiskProxyImpl implements DiskProxy {
    private static final String TAG = DiskProxyImpl.class.getName();

    private Context context;
    private Logger logger;

    public DiskProxyImpl(Context context, Logger logger) {
        this.context = context;
        this.logger = logger;
    }

    @Override
    public void writeToFile(String name, Serializable serializable) {
        try {
            FileOutputStream fileOutputStream = context.openFileOutput(name, Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(serializable);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            logger.logError(TAG, "Error writing to file: " + name, e);
        }
    }

    @Override
    public <T extends Serializable> T readFromFile(String name, Class<T> clazz) {
        try {
            FileInputStream fileInputStream = context.openFileInput(name);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            return clazz.cast(objectInputStream.readObject());
        } catch (Exception e) {
            logger.logError(TAG, "Error reading from file: " + name, e);
            return null;
        }
    }

    @Override
    public void deleteFile(String name) {
        context.deleteFile(name);
    }
}
