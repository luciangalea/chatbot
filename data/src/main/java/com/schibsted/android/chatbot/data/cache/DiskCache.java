package com.schibsted.android.chatbot.data.cache;

import android.Manifest;
import android.content.SharedPreferences;

import com.schibsted.android.chatbot.data.cache.disk.DiskProxy;
import com.schibsted.android.chatbot.data.entity.ChatMessage;
import com.schibsted.android.chatbot.data.entity.ChatMessagesResult;

import io.reactivex.Observable;

/**
 * Created by Lucian on 1/12/2017.
 */

public class DiskCache implements Cache {
    private static final String KEY_NAME = "keyName";
    private static final String MESSAGES = "messages";

    private SharedPreferences sharedPreferences;
    private DiskProxy diskProxy;

    public DiskCache(SharedPreferences sharedPreferences, DiskProxy diskProxy) {
        this.sharedPreferences = sharedPreferences;
        this.diskProxy = diskProxy;
    }

    @Override
    public Observable<String> getName() {
        return Observable.create(e -> {
            e.onNext(sharedPreferences.getString(KEY_NAME, ""));
            e.onComplete();
        });
    }

    @Override
    public void storeName(String name) {
        sharedPreferences.edit().putString(KEY_NAME, name).apply();
    }

    @Override
    public void clear() {
        sharedPreferences.edit().clear().apply();
        diskProxy.deleteFile(MESSAGES);
    }

    @Override
    public void storeMessages(ChatMessagesResult messages) {
        diskProxy.writeToFile(MESSAGES, messages);
    }

    @Override
    public Observable<ChatMessagesResult> getMessages() {
        return Observable.create(e -> {
            ChatMessagesResult result = diskProxy.readFromFile(MESSAGES, ChatMessagesResult.class);
            e.onNext(result != null ? result : ChatMessagesResult.dummy());
            e.onComplete();
        });

    }
}
