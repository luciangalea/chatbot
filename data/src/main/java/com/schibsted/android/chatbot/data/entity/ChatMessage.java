package com.schibsted.android.chatbot.data.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Lucian on 1/12/2017.
 */

public class ChatMessage implements Serializable {
    @SerializedName("username")
    private String username;
    @SerializedName("content")
    private String content;
    @SerializedName("userImage_url")
    private String imageUrl;
    @SerializedName("time")
    private String time;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
