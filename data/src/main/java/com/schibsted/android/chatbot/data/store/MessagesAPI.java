package com.schibsted.android.chatbot.data.store;

import com.schibsted.android.chatbot.data.entity.ChatMessagesResult;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Lucian on 1/12/2017.
 */

public interface MessagesAPI {
    @GET("/rocket-interview/chat.json")
    Observable<ChatMessagesResult> getMessages();
}
