package com.schibsted.android.chatbot.data.configuration;

/**
 * Created by Lucian on 1/12/2017.
 */

public interface Configuration {
    long getCacheDurationMillis();
}
