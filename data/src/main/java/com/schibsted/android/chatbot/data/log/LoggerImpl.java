package com.schibsted.android.chatbot.data.log;

import android.util.Log;

/**
 * Created by Lucian on 1/12/2017.
 */

public class LoggerImpl implements Logger {
    @Override
    public void logError(String tag, String message, Throwable throwable) {
        Log.e(tag, message, throwable);
    }
}
