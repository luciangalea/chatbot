package com.schibsted.android.chatbot.bindings;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.schibsted.android.chatbot.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Lucian on 1/13/2017.
 */

public class ImageLoadingBindings {

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Picasso.with(view.getContext())
                .load(imageUrl)
                .placeholder(R.mipmap.ic_launcher)
                .into(view);
    }
}
