package com.schibsted.android.chatbot.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schibsted.android.chatbot.BR;
import com.schibsted.android.chatbot.R;
import com.schibsted.android.chatbot.data.entity.ChatMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lucian on 1/13/2017.
 */

public class ChatMessagesAdapter extends RecyclerView.Adapter<ChatMessagesAdapter.ViewHolder> {
    private List<ChatMessage> messages = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_message, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.viewDataBinding.setVariable(BR.message, messages.get(position));
        holder.viewDataBinding.executePendingBindings();;
    }

    @Override
    public int getItemCount() {
        return messages != null ? messages.size() : 0;
    }

    public void setMessages(List<ChatMessage> messages) {
        this.messages.addAll(messages);
    }

    public void addMessage(ChatMessage chatMessage) {
        messages.add(chatMessage);
        notifyItemInserted(messages.size());
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding viewDataBinding;

        ViewHolder(View itemView) {
            super(itemView);

            viewDataBinding = DataBindingUtil.bind(itemView);
        }
    }
}
