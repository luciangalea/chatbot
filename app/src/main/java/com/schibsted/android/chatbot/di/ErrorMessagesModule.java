package com.schibsted.android.chatbot.di;

import android.content.Context;

import com.schibsted.android.chatbot.R;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lucian on 1/13/2017.
 */

@Module
public class ErrorMessagesModule {
    @Provides
    @Named("nameUsedError")
    String providesNameUsedErrorMessage(Context context) {
        return context.getString(R.string.name_is_already_used);
    }

    @Provides
    @Named("loginError")
    String providesLoginErrorMessage(Context context) {
        return context.getString(R.string.login_error);
    }

    @Provides
    @Named("messageSendError")
    String providesMessageSendError(Context context) {
        return context.getString(R.string.could_not_send_message);
    }

    @Provides
    @Named("logoutErrorMessage")
    String providesLogoutErrorMessage(Context context) {
        return context.getString(R.string.error_log_out);
    }
}
