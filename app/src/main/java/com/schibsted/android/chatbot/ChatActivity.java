package com.schibsted.android.chatbot;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.schibsted.android.chatbot.adapters.ChatMessagesAdapter;
import com.schibsted.android.chatbot.data.entity.ChatMessage;
import com.schibsted.android.chatbot.presenter.ChatPresenter;
import com.schibsted.android.chatbot.view.ChatView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatActivity extends AppCompatActivity implements ChatView {
    public static final String ARG_NAME = "argName";
    @BindView(R.id.recycler_messages)
    RecyclerView messagesRecyclerView;
    @BindView(R.id.container)
    ViewGroup container;
    @BindView(R.id.input_message)
    TextInputLayout messageInputLayout;
    @BindView(R.id.edt_message)
    EditText messageEditText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Inject
    ChatPresenter presenter;

    private ChatMessagesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle(getIntent().getStringExtra(ARG_NAME));

        adapter = new ChatMessagesAdapter();
        messagesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        messagesRecyclerView.setAdapter(adapter);

        ((ChatApplication) getApplication()).getApplicationComponent().inject(this);
        messageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.onTextEntered();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.detachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            presenter.onLogoutClicked();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_send)
    public void onSendClicked() {
        presenter.onTextSent(messageEditText.getText().toString());
    }

    @Override
    public void renderMessages(List<ChatMessage> messages) {
        adapter.setMessages(messages);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void appendMessage(ChatMessage chatMessage) {
        adapter.addMessage(chatMessage);
        messagesRecyclerView.post(() ->   messagesRecyclerView.getLayoutManager().scrollToPosition(adapter.getItemCount() - 1));
    }

    @Override
    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void renderGetMessagesError() {
        Snackbar.make(container, getString(R.string.error_getting_message), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void renderError(String errorMessage) {
        Snackbar.make(container, errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void renderMessageRequiredError() {
        messageInputLayout.setError(getString(R.string.error_field_required));
    }

    @Override
    public void clearErrors() {
        messageInputLayout.setError(null);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void navigateToLoginScreen() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
