package com.schibsted.android.chatbot.view;

import com.schibsted.android.chatbot.data.entity.ChatMessage;

import java.util.List;

/**
 * Created by Lucian on 1/13/2017.
 */

public interface ChatView extends View {
    void renderMessages(List<ChatMessage> messages);

    void renderGetMessagesError();

    void renderError(String errorMessage);

    void renderMessageRequiredError();

    void appendMessage(ChatMessage chatMessage);

    void hideKeyboard();

    void clearErrors();

    void showProgress();

    void hideProgress();

    void navigateToLoginScreen();
}
