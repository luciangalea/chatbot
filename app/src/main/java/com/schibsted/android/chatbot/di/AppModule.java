package com.schibsted.android.chatbot.di;

import android.app.Application;
import android.content.Context;

import com.schibsted.android.chatbot.data.configuration.Configuration;
import com.schibsted.android.chatbot.data.configuration.ConfigurationImpl;
import com.schibsted.android.chatbot.domain.formatter.TimeFormatter;
import com.schibsted.android.chatbot.domain.formatter.TimeFormatterImpl;
import com.schibsted.android.chatbot.domain.interactor.thread.InteractorThread;
import com.schibsted.android.chatbot.domain.validator.EmptyFieldValidator;
import com.schibsted.android.chatbot.domain.validator.NameValidator;
import com.schibsted.android.chatbot.domain.validator.Validator;

import org.threeten.bp.format.DateTimeFormatter;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Lucian on 1/13/2017.
 */

@Module
public class AppModule {
    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    Context providesContext() {
        return application;
    }

    @Provides
    @Named("observeThread")
    InteractorThread providesObserveThread() {
        return AndroidSchedulers::mainThread;
    }

    @Provides
    @Named("subscribeThread")
    InteractorThread providesSubscribeThread() {
        return Schedulers::io;
    }

    @Provides
    @Named("emptyFieldValidator")
    Validator providesEmptyFieldValidator() {
        return new EmptyFieldValidator();
    }

    @Provides
    @Named("nameValidator")
    Validator providesNameValidator() {
        return new NameValidator();
    }

    @Provides
    Configuration providesConfiguration() {
        return new ConfigurationImpl();
    }

    @Provides
    @Singleton
    TimeFormatter providesTimeFormatter() {
        return new TimeFormatterImpl();
    }

}
