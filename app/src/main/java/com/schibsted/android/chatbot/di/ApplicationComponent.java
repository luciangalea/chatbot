package com.schibsted.android.chatbot.di;

import com.schibsted.android.chatbot.ChatActivity;
import com.schibsted.android.chatbot.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Lucian on 1/13/2017.
 */

@Singleton
@Component(modules = {AppModule.class, DataModule.class, ErrorMessagesModule.class})
public interface ApplicationComponent {
    void inject(LoginActivity activity);

    void inject(ChatActivity activity);
}
