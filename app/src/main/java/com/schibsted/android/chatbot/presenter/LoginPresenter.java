package com.schibsted.android.chatbot.presenter;

import com.schibsted.android.chatbot.domain.interactor.GetNameInteractor;
import com.schibsted.android.chatbot.domain.interactor.LoginInteractor;
import com.schibsted.android.chatbot.domain.validator.Validator;
import com.schibsted.android.chatbot.view.LoginView;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * Created by Lucian on 1/12/2017.
 */

public class LoginPresenter extends BasePresenter<LoginView> {
    private Validator emptyFieldValidator;
    private Validator nameValidator;
    private LoginInteractor loginInteractor;
    private GetNameInteractor getNameInteractor;

    private CompositeDisposable disposable = new CompositeDisposable();

    @Inject
    LoginPresenter(@Named("nameValidator") Validator nameValidator,
                   @Named("emptyFieldValidator") Validator emptyFieldValidator,
                   LoginInteractor loginInteractor,
                   GetNameInteractor getNameInteractor) {
        this.nameValidator = nameValidator;
        this.emptyFieldValidator = emptyFieldValidator;
        this.loginInteractor = loginInteractor;
        this.getNameInteractor = getNameInteractor;
    }

    @Override
    public void attachView(LoginView view) {
        super.attachView(view);
        disposable.add(getNameInteractor.execute().subscribe(response -> {
            if (response.success() && response.data().length() > 0) {
                view.navigateToChatScreen(response.data());
            }
        }));
    }

    public void onSignInClicked(String name) {
        if (validateName(name)) {
            disposable.add(loginInteractor.withData(name).execute()
                    .subscribe(response -> {
                        if (response.success()) {
                            view.navigateToChatScreen(name);
                        } else {
                            view.renderLoginError(response.errorMessage());
                        }
                    }, error -> {
                        view.renderLoginError(error.getMessage());
                    }));
        }
    }

    private boolean validateName(String name) {
        if (!emptyFieldValidator.isValid(name)) {
            view.renderEmptyNameError();
            return false;
        } else if (!nameValidator.isValid(name)) {
            view.renderNameNotValidError();
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void cleanup() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    public void onLoginNameChanged() {
        view.clearErrors();
    }
}
