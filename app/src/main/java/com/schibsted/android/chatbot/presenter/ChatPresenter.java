package com.schibsted.android.chatbot.presenter;

import com.schibsted.android.chatbot.domain.interactor.AddMessageInteractor;
import com.schibsted.android.chatbot.domain.interactor.GetMessagesInteractor;
import com.schibsted.android.chatbot.domain.interactor.LogoutInteractor;
import com.schibsted.android.chatbot.domain.validator.Validator;
import com.schibsted.android.chatbot.view.ChatView;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Lucian on 1/13/2017.
 */

public class ChatPresenter extends BasePresenter<ChatView> {
    private GetMessagesInteractor getMessagesInteractor;
    private AddMessageInteractor addMessageInteractor;
    private LogoutInteractor logoutInteractor;
    private Validator emptyFieldValidator;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    ChatPresenter(GetMessagesInteractor getMessagesInteractor, AddMessageInteractor addMessageInteractor,
                  LogoutInteractor logoutInteractor,
                  @Named("emptyFieldValidator") Validator emptyFieldValidator) {
        this.getMessagesInteractor = getMessagesInteractor;
        this.addMessageInteractor = addMessageInteractor;
        this.logoutInteractor = logoutInteractor;
        this.emptyFieldValidator = emptyFieldValidator;
    }

    @Override
    public void attachView(ChatView view) {
        super.attachView(view);
        view.showProgress();
        compositeDisposable.add(
                getMessagesInteractor.execute().subscribe(messages -> {
                            view.hideProgress();
                            view.renderMessages(messages);

                        },
                        error -> {
                            view.hideProgress();
                            view.renderGetMessagesError();
                        }
                )
        );
    }

    public void onTextSent(String message) {
        if (!emptyFieldValidator.isValid(message)) {
            view.renderMessageRequiredError();
        } else {
            view.showProgress();
            compositeDisposable.add(
                    addMessageInteractor.withData(message).execute().subscribe(response -> {
                        view.hideProgress();
                        if (response.success()) {
                            view.hideKeyboard();
                            view.appendMessage(response.data());
                        } else {
                            view.renderError(response.errorMessage());
                        }
                    }, error -> {
                        view.hideProgress();
                        view.renderError(error.getMessage());
                    })
            );
        }
    }

    public void onTextEntered() {
        view.clearErrors();
    }

    public void onLogoutClicked() {
        compositeDisposable.add(logoutInteractor.execute().subscribe(response -> {
            if (response.success()) {
                view.navigateToLoginScreen();
            } else {
                view.renderError(response.errorMessage());
            }
        }, error -> view.renderError(error.getMessage())));
    }

    @Override
    protected void cleanup() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
