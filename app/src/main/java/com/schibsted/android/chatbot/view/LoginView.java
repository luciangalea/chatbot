package com.schibsted.android.chatbot.view;

/**
 * Created by Lucian on 1/12/2017.
 */

public interface LoginView extends View {
    void renderEmptyNameError();

    void renderNameNotValidError();

    void renderLoginError(String message);

    void clearErrors();

    void navigateToChatScreen(String name);
}
