package com.schibsted.android.chatbot.di;

import android.content.Context;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.schibsted.android.chatbot.R;
import com.schibsted.android.chatbot.data.cache.Cache;
import com.schibsted.android.chatbot.data.cache.DiskCache;
import com.schibsted.android.chatbot.data.cache.MemoryCache;
import com.schibsted.android.chatbot.data.cache.disk.DiskProxy;
import com.schibsted.android.chatbot.data.cache.disk.DiskProxyImpl;
import com.schibsted.android.chatbot.data.log.Logger;
import com.schibsted.android.chatbot.data.log.LoggerImpl;
import com.schibsted.android.chatbot.data.store.CloudMessagesStore;
import com.schibsted.android.chatbot.data.store.CouldUserStore;
import com.schibsted.android.chatbot.data.store.MessagesAPI;
import com.schibsted.android.chatbot.data.store.MessagesStore;
import com.schibsted.android.chatbot.data.store.UserStore;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lucian on 1/13/2017.
 */

@Module
public class DataModule {

    private static final String SCHIBSTED_APP = "SchibstedApp";

    @Provides
    @Singleton
    MessagesStore providesMessagesStore(Context context) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(context.getString(R.string.base_url))
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return new CloudMessagesStore(retrofit.create(MessagesAPI.class));
    }

    @Provides
    @Singleton
    UserStore providesUserStore() {
        return new CouldUserStore();
    }

    @Provides
    @Singleton
    Logger providesLogger() {
        return new LoggerImpl();
    }

    @Provides
    @Singleton
    DiskProxy providesDiskProxy(Context context, Logger logger) {
        return new DiskProxyImpl(context, logger);
    }

    @Provides
    @Singleton
    @Named("memory")
    Cache providesMemoryCache() {
        return new MemoryCache();
    }

    @Provides
    @Singleton
    @Named("disk")
    Cache providesDiskCache(Context context, DiskProxy diskProxy) {
        return new DiskCache(context.getSharedPreferences(SCHIBSTED_APP, Context.MODE_PRIVATE), diskProxy);
    }
}
