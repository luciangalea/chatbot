package com.schibsted.android.chatbot;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.schibsted.android.chatbot.presenter.LoginPresenter;
import com.schibsted.android.chatbot.view.LoginView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A login screen that offers login via Name & Surname.
 */
public class LoginActivity extends AppCompatActivity implements LoginView {

    @BindView(R.id.name)
    EditText nameEditText;
    @BindView(R.id.input_name)
    TextInputLayout nameInputLayout;
    @Inject
    LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        ((ChatApplication) getApplication()).getApplicationComponent().inject(this);
        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.onLoginNameChanged();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.detachView();
    }

    @OnClick(R.id.sign_in_button)
    public void attemptLogin() {
        presenter.onSignInClicked(nameEditText.getText().toString());
    }

    @Override
    public void renderEmptyNameError() {
        nameInputLayout.setError(getString(R.string.error_field_required));
    }

    @Override
    public void renderNameNotValidError() {
        nameInputLayout.setError(getString(R.string.name_not_valid));
    }

    @Override
    public void renderLoginError(String message) {
        nameInputLayout.setError(message);
    }

    @Override
    public void clearErrors() {
        nameInputLayout.setError(null);
    }

    @Override
    public void navigateToChatScreen(String name) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(ChatActivity.ARG_NAME, name);
        startActivity(intent);

        finish();
    }
}

