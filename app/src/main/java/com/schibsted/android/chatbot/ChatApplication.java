package com.schibsted.android.chatbot;

import android.app.Application;

import com.jakewharton.threetenabp.AndroidThreeTen;
import com.schibsted.android.chatbot.di.AppModule;
import com.schibsted.android.chatbot.di.ApplicationComponent;
import com.schibsted.android.chatbot.di.DaggerApplicationComponent;
import com.schibsted.android.chatbot.di.DataModule;
import com.schibsted.android.chatbot.di.ErrorMessagesModule;

/**
 * Created by Lucian on 1/13/2017.
 */

public class ChatApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
    }

    public ApplicationComponent getApplicationComponent() {
        if (applicationComponent == null) {
            applicationComponent = createApplicationComponent();
        }
        return applicationComponent;
    }

    private ApplicationComponent createApplicationComponent() {
        return DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .dataModule(new DataModule())
                .errorMessagesModule(new ErrorMessagesModule())
                .build();
    }
}
